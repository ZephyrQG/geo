<?php

namespace Zephyr\Geo\ImagesBundle\Flickr;


/**
 * Description of Flickr
 *
 * @author Kanea
 */
class Manager {
	
	/**@var DPZFlickr  */
	private $flickObject;

    /**
     * @param $key
     * @param $secret
     * @internal param \Zephyr\Geo\ImagesBundle\Flickr\type $apitoken
     * @internal param \Zephyr\Geo\ImagesBundle\Flickr\type $apikey
     */
	public function __construct($key, $secret)
	{
		$this->flickObject = new DPZFlickr($key, $secret, 'oob');	
	}
	public function auth() {
		$this->flickObject->authenticate('read');
	}

    /**
     *
     * @param type $ville
     * @param type $latitude
     * @param type $longitude
     * @param int|\Zephyr\Geo\ImagesBundle\Flickr\type $limit
     * @return \Zephyr\Geo\ImagesBundle\Flickr\Collection
     */
	public function getImages($ville, $latitude, $longitude, $limit=5)
	{
		//$this->auth();
		
		/* voir http://www.flickr.com/services/api/flickr.photos.search.html*/
		$photos = new Collection();
		foreach($this->flickObject->call('flickr.photos.search', [
		  /*World level is 1
			Country is ~3
			Region is ~6
			City is ~11
			Street is ~16*/
		  'text'=>$ville,
		  'accuracy'=>11,
		  'content_type'=>1,
		  'media'=>'photos',
		  'per_page'=>$limit,
		  //'geo_context'=>'0',
		  /* voir http://www.flickr.com/services/api/flickr.photos.licenses.getInfo.html*/
		  'licence'=>'4,5,6',
		  'lat'=>$latitude,
		  'lon'=>$longitude,
		  'extras'=>
		      "date_upload, date_taken, owner_name, icon_server, original_format,
			  last_update, geo, tags, machine_tags, o_dims, views, media, path_alias,
			  url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l, url_o"
		])['photos']['photo'] as $photo){
			$photos->add(new Image($photo));
		}
		return $photos;		  
	}
}

?>
