<?php

namespace Zephyr\Geo\ImagesBundle\Flickr;

/**
 * Description of Collection
 *
 * @author Nicolas de Marqué
 */
class Collection implements \Iterator, \Countable{

	/** @var array $images */
	private $images = [];
	/**
	 * @param \Zephyr\Geo\ImagesBundle\Flickr\Image $image
	 * @return \Zephyr\Geo\ImagesBundle\Flickr\Collection
	 */
	public function add(Image $image){$this->images[]=$image;return $this;}

	/** @return \Zephyr\Geo\ImagesBundle\Flickr\Image $image */
	public function get(){$image = $this->current(); $this->next(); return $image;}

	/** @return Image */
	public function current() { return current($this->images);}
	public function key() { return key($this->images);}
	public function next() { next($this->images);}
	public function rewind() { reset($this->images);}
	public function valid() { reset($this->images);}
	public function count() { return count($this->images);}
}

?>
