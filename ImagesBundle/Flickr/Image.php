<?php

namespace Zephyr\Geo\ImagesBundle\Flickr;

/**
 * Description of Image
 *
 * @author Kanea
 */
class Image
{

    private $format;
    private $size;
    private $id;
    private $owner;
    private $secret;
    private $server;
    private $farm;
    private $title;
    private $ispublic;
    private $isfriend;
    private $isfamily;
    private $dateupload;
    private $datetaken;
    private $datetakengranularity;
    private $ownername;
    private $iconserver;
    private $iconfarm;
    private $originalsecret;
    private $originalformat;
    private $lastupdate;
    private $latitude;
    private $longitude;
    private $accuracy;
    private $context;
    private $place_id;
    private $woeid;
    private $geo_is_family;
    private $geo_is_friend;
    private $geo_is_contact;
    private $geo_is_public;
    private $tags;
    private $machine_tags;
    private $o_width;
    private $o_height;
    private $views;
    private $media;
    private $media_status;
    private $pathalias;
    private $url_sq;
    private $height_sq;
    private $width_sq;
    private $url_t;
    private $height_t;
    private $width_t;
    private $url_s;
    private $height_s;
    private $width_s;
    private $url_q;
    private $height_q;
    private $width_q;
    private $url_m;
    private $height_m;
    private $width_m;
    private $url_n;
    private $height_n;
    private $width_n;
    private $url_z;
    private $height_z;
    private $width_z;
    private $url_c;
    private $height_c;
    private $width_c;
    private $url_l;
    private $height_l;
    private $width_l;
    private $url_o;
    private $height_o;
    private $width_o;

    public function __construct( $array )
    {
        foreach ( $array as $key => $value )
            if ( property_exists( $this, $key ) )
                $this->$key = $value;
            else
                throw new \Exception( "La propriété $key n'est pas défini dans la class Flickr\Image" . print_r( $array, 1 ) );
        $this->setSize( 'o' );
        $this->setFormat( $this->originalformat );
    }

    public function getLargestImageUrl()
    {
        foreach ( ['url_o', 'url_l', 'url_c', 'url_z', 'url_n', 'url_q', 'url_s', 'url_t', 'url_sq'] as $prop )
        {
            if ( $this->$prop == NULL or $this->$prop == "" )
                continue;
            return $this->$prop;
        }
        throw new \Exception( "Aucune Image" );
    }

    public function getUrl()
    {
        return "http://farm" . $this->farm . ".staticflickr.com/" . $this->server . "/" . $this->id . "_" . $this->secret . "_" . $this->size . "." . $this->format;
    }

    public function getFormat()
    {
        return $this->format;
    }

    /**
     * 
     * @param type $format
     * @return \Zephyr\Geo\ImagesBundle\Flickr\Image
     */
    public function setFormat( $format )
    {
        $this->format = $format;
        return $this;
    }

    public function getSize()
    {
        return $this->size;
    }

    /**
     * Taille :
     *  s	petit carré 75x75
     *  q	large square 150x150
     *  t	miniature, côté le plus long de 100
     *  m	petit, côté le plus long de 240
     *  n	small, 320 on longest side
     *  -	moyen, côté le plus long de 500
     *  z	Moyen 640, côté le plus long de 640
     *  c	moyen 800, 800 sur la longueur†
     *  b	grand, côté le plus long de 1 024*
     *  o	image d'origine, jpg, gif ou png selon le format source
     * 
     * @param type $size
     * @return \Zephyr\Geo\ImagesBundle\Flickr\Image
     */
    public function setSize( $size )
    {
        $this->size = $size;
        if ( $size != "o" )
            $this->format = "jpg";
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function getSecret()
    {
        return $this->secret;
    }

    public function getServer()
    {
        return $this->server;
    }

    public function getFarm()
    {
        return $this->farm;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getIspublic()
    {
        return $this->ispublic;
    }

    public function getIsfriend()
    {
        return $this->isfriend;
    }

    public function getIsfamily()
    {
        return $this->isfamily;
    }

    public function getDateupload()
    {
        return $this->dateupload;
    }

    public function getDatetaken()
    {
        return $this->datetaken;
    }

    public function getDatetakengranularity()
    {
        return $this->datetakengranularity;
    }

    public function getOwnername()
    {
        return $this->ownername;
    }

    public function getIconserver()
    {
        return $this->iconserver;
    }

    public function getIconfarm()
    {
        return $this->iconfarm;
    }

    public function getOriginalsecret()
    {
        return $this->originalsecret;
    }

    public function getOriginalformat()
    {
        return $this->originalformat;
    }

    public function getLastupdate()
    {
        return $this->lastupdate;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function getAccuracy()
    {
        return $this->accuracy;
    }

    public function getContext()
    {
        return $this->context;
    }

    public function getPlace_id()
    {
        return $this->place_id;
    }

    public function getWoeid()
    {
        return $this->woeid;
    }

    public function getGeo_is_family()
    {
        return $this->geo_is_family;
    }

    public function getGeo_is_friend()
    {
        return $this->geo_is_friend;
    }

    public function getGeo_is_contact()
    {
        return $this->geo_is_contact;
    }

    public function getGeo_is_public()
    {
        return $this->geo_is_public;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function getMachine_tags()
    {
        return $this->machine_tags;
    }

    public function getO_width()
    {
        return $this->o_width;
    }

    public function getO_height()
    {
        return $this->o_height;
    }

    public function getViews()
    {
        return $this->views;
    }

    public function getMedia()
    {
        return $this->media;
    }

    public function getMedia_status()
    {
        return $this->media_status;
    }

    public function getPathalias()
    {
        return $this->pathalias;
    }

    public function getUrl_sq()
    {
        return $this->url_sq;
    }

    public function getHeight_sq()
    {
        return $this->height_sq;
    }

    public function getWidth_sq()
    {
        return $this->width_sq;
    }

    public function getUrl_t()
    {
        return $this->url_t;
    }

    public function getHeight_t()
    {
        return $this->height_t;
    }

    public function getWidth_t()
    {
        return $this->width_t;
    }

    public function getUrl_s()
    {
        return $this->url_s;
    }

    public function getHeight_s()
    {
        return $this->height_s;
    }

    public function getWidth_s()
    {
        return $this->width_s;
    }

    public function getUrl_q()
    {
        return $this->url_q;
    }

    public function getHeight_q()
    {
        return $this->height_q;
    }

    public function getWidth_q()
    {
        return $this->width_q;
    }

    public function getUrl_m()
    {
        return $this->url_m;
    }

    public function getHeight_m()
    {
        return $this->height_m;
    }

    public function getWidth_m()
    {
        return $this->width_m;
    }

    public function getUrl_n()
    {
        return $this->url_n;
    }

    public function getHeight_n()
    {
        return $this->height_n;
    }

    public function getWidth_n()
    {
        return $this->width_n;
    }

    public function getUrl_z()
    {
        return $this->url_z;
    }

    public function getHeight_z()
    {
        return $this->height_z;
    }

    public function getWidth_z()
    {
        return $this->width_z;
    }

    public function getUrl_l()
    {
        return $this->url_l;
    }

    public function getHeight_l()
    {
        return $this->height_l;
    }

    public function getWidth_l()
    {
        return $this->width_l;
    }

    public function getUrl_o()
    {
        return $this->url_o;
    }

    public function getHeight_o()
    {
        return $this->height_o;
    }

    public function getWidth_o()
    {
        return $this->width_o;
    }

}