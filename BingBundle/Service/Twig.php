<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Arnaud
 * Date: 10/07/13
 * Time: 10:15
 * To change this template use File | Settings | File Templates.
 */

namespace Zephyr\Geo\BingBundle\Service;


use Symfony\Component\DependencyInjection\ContainerInterface;

class Twig extends \Twig_Extension {

    /** @var ContainerInterface */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            'bingMapsLib' => new \Twig_Filter_Method(
                $this,
                'bingMapsLib',
                [
                    'needs_environment' => false,
                    'is_safe' => array('html')
                ]
            )
        );
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'bingMapsLib' => new \Twig_Function_Method(
                $this,
                'bingMapsLib',
                [
                    'needs_environment' => false,
                    'needs_context' => true,
                    'is_safe' => array('html')
                ]
            )
        );
    }

    /**
     * Returns script tag with Bing Maps library URL.
     *
     * @param bool $tag False if you only want the url
     * @return string Script tag or url depending onf param
     */
    public function bingMapsLib($tag = true)
    {
        $url = $this->container->get("zephyr.geo.bingmaps")->getJavascriptLibraryUrl();
        return $tag ? '<script type="text/javascript" src="'.$url.'"></script>' : $url;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return "zephyr.geo.bing.twig.extension";
    }
}