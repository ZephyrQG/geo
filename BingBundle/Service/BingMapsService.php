<?php

/**
 * Created by JetBrains PhpStorm.
 * User: Arnaud
 * Date: 10/07/13
 * Time: 09:35
 * To change this template use File | Settings | File Templates.
 */

namespace Zephyr\Geo\BingBundle\Service;

use Clicdanstaville\FrontendBundle\Entity\City;
use SimpleXMLElement;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Zephyr\Geo\Data\Map;

class BingMapsService
{

    private $container;
    private $mkt;
    private $key;

    /**
     * @param ContainerInterface $container
     * @param string $mkt
     */
    public function __construct( ContainerInterface $container, $mkt = "fr-FR" )
    {
        $this->container = $container;
        $this->mkt = $mkt;
        $this->key = "Aisz7GgDpEaGoKpd8on-cNboKat0zrQa2GeJJTJiMpfLW361Amm_L2KdiENd4Li8";
    }

    /**
     * @return string
     */
    public function getJavascriptLibraryUrl()
    {
        return 'https://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0&s=1&mkt=' . $this->mkt;
    }

    /**
     * @return $this
     */
    public function createMap()
    {
        return Map::create()->setContainer( $this->container );
    }

    /**
     * @param string $country
     * @param City $city
     * @param $addressLine
     * @return array
     */
    public function getGeolocalisation( City $city, $addressLine, $country = "FR" )
    {
        // URL of Bing Maps REST Services Locations API
        $baseURL = "http://dev.virtualearth.net/REST/v1/Locations";

        // Create variables for search parameters (encode all spaces by specifying '%20' in the URI)
        $addressLine = str_ireplace( " ", "%20", $addressLine );
        $zipcode = $city->getZipCode();
        $locality = str_ireplace( " ", "%20", $city->getName() );

        // Compose URI for Locations API request
        $findURL = $baseURL . "/" . $country . "/" . $zipcode . "/" . $locality . "/" . $addressLine . "?output=xml&key=" . $this->key;

        // create an XML element based on the XML string
        $output = file_get_contents( $findURL );

        // create an XML element based on the XML string
        $response = new SimpleXMLElement( $output );

        var_dump( $response );

        $latitude = "0";
        $longitude = "0";

        if ( $response->ResourceSets->ResourceSet->EstimatedTotal != "0" ) {
            // Extract data (e.g. latitude and longitude) from the results
            $latitude = $response->ResourceSets->ResourceSet->Resources->Location->Point->Latitude;
            $longitude = $response->ResourceSets->ResourceSet->Resources->Location->Point->Longitude;
        }
        return array(
            "latitude" => $latitude,
            "longitude" => $longitude
        );
    }

}
