<?php

namespace Zephyr\Geo\Data;

/**
 * Description de la class Marqueur
 * @author Kanea
 */

/**
 * Class Marker
 * @package Zephyr\Geo\Data
 */
class Marker extends POI
{

    use CreateStatic;

    /** @var Icon $icon */
    protected $icon;

    /** @var Modal $modale */
    protected $modal;

    /**
     * @var
     */
    protected $title;

    /**
     * @var
     */
    protected $content;

    /**
     * @var
     */
    protected $cssclass;

    /**
     * @var bool
     */
    protected $active = true;

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param $title
     * @return $this
     */
    public function setTitle( $title )
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return Icon
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param Icon $icon
     * @return $this
     */
    public function setIcon( Icon $icon )
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return Modal
     */
    public function getModal()
    {
        return $this->modal;
    }

    /**
     * @param Modal $modale
     * @return $this
     */
    public function setModal( Modal $modale )
    {
        $this->modal = $modale;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param $content
     * @return $this
     */
    public function setContent( $content )
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCssclass()
    {
        return $this->cssclass;
    }

    /**
     * @param $cssclass
     * @return $this
     */
    public function setCssclass( $cssclass )
    {
        $this->cssclass = $cssclass;
        return $this;
    }

    /**
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param $actif
     * @return $this
     */
    public function setActive( $actif )
    {
        $this->active = $actif;
        return $this;
    }

}
