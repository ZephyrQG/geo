<?php

namespace Zephyr\Geo\Data;

use Symfony\Component\DependencyInjection\ContainerAware;

/**
 * Description of Point
 *
 * @author Kanea
 */
class POI extends ContainerAware
{

    protected $latitude;
    protected $longitude;

    /**
     * @param $longitude
     * @param $latitude
     */
    public function __construct( $longitude, $latitude )
    {
        $this->longitude = $longitude;
        $this->latitude = $latitude;
    }

    public function getLatitude()
    {
        return $this->latitude;
    }

    public function getY()
    {
        return $this->latitude;
    }

    public function setLatitude( $latitude )
    {
        $this->latitude = $latitude;
        return $this;
    }

    public function getLongitude()
    {
        return $this->longitude;
    }

    public function getX()
    {
        return $this->longitude;
    }

    public function setLongitude( $longitude )
    {
        $this->longitude = $longitude;
        return $this;
    }

}
