<?php

namespace Zephyr\Geo\Data;

/**
 * Description de tools
 *
 * @author Kanea
 */
class Tools {

	protected $movable = false;
	protected $zoom = false;
	protected $position = "rb";

	/** var Point $anchor */
	protected $anchor;
	protected $direction;

    public function __construct()
    {
		$this->anchor = new POI(5,15);
	}
	public function movable() {
		return $this->movable;
	}

	public function setMovable($deplacement)
    {
		$this->movable = $deplacement;
		return $this;
	}

	public function Zoom()
    {
		return $this->zoom;
	}

	public function setZoom($zoom)
    {
		$this->zoom = $zoom;
		return $this;
	}

	public function Position()
    {
		return $this->position;
	}

	/**
	 * Soit rb, lb, rt, lt
	 * @param string $position
	 * @return \Zephyr\Geo\MappyBundle\Data\Tools
	 */
	public function setPosition($position)
    {
		$this->position = $position;
		return $this;
	}

	public function getAnchor()
    {
		return $this->anchor;
	}

	public function setCoords($x, $y)
    {
		return $this->setAnchor(new POI($x, $y));
	}

	public function setAnchor(POI $ancrage)
    {
		$this->anchor = $ancrage;
		return $this;
	}

	public function getDirection()
    {
		return $this->direction;
	}
	/**
	 * La direction est soit :
	 *  - "vertical"
	 *  - "horizontal"
	 *  - "none"
	 * @param type $direction
	 * @return \Zephyr\Geo\MappyBundle\Data\Tools
	 */
	public function setDirection($direction)
    {
		$this->direction = $direction;
		return $this;
	}

}