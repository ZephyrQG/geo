<?php

namespace Zephyr\Geo\Data;

use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Carte
 *
 * @author Kanea
 */
class Map extends ContainerAware
{

    use CreateStatic;

    /**
     * @var string
     */
    protected $id;

    /**
     * @var
     */
    protected $markers;

    /**
     * @var
     */
    protected $zoom;

    /**
     * @var
     */
    protected $center;

    /**
     * @var string
     */
    protected $width = "100%";

    /**
     * @var string
     */
    protected $height = "100%";

    /**
     * @var int
     */
    protected $iconLabel = 1;

    /**
     * @var bool
     */
    protected $static = false;

    /**
     * @var string
     */
    protected $trigger = "$(document).ready";

    /**
     * @var string
     */
    protected $class = "";

    /**
     * @var bool
     */
    static protected $includedLibs = false;

    /** @var Tools $outils */
    protected $tools = false;

    /**
     *
     */
    public function __construct()
    {
        $this->id = preg_replace( '/\./', '_', microtime( true ) ) . "_" . rand( 0, 1000 );
    }

    /**
     * @return string
     */
    public function getLibs()
    {
        $service = $this->container->get( 'zephyr.geo.mappy' );
        if ( self::$includedLibs )
            return "";
        self::$includedLibs = true;
        return $service->getJavascriptLibraryUrl();
    }

    /**
     * @return string
     */
    public function render()
    {
        /* @var $twig \Twig_Environment */
        $twig = $this->container->get( "twig" );
        return $twig->render( "ZephyrGeoMappyBundle:Carte:carte.html.twig", ['carte' => $this] );
    }

    /**
     * @param $longitude
     * @param $latitude
     * @param string $title
     * @param string $cssclass
     * @param bool $decorationMappy
     * @param Icon $icon
     * @return $this
     */
    public function addDisabledMarker( $longitude, $latitude, $title = "", $cssclass = "", $decorationMappy = true, Icon $icon = null )
    {
        return $this->createMarker( $longitude, $latitude, $title, "", false, $cssclass, $decorationMappy, $icon );
    }

    /**
     * @param $longitude
     * @param $latitude
     * @param $title
     * @param $content
     * @param bool $enabled
     * @param string $cssclass
     * @param bool $decorationMappy
     * @param Icon $icon
     * @return $this
     */
    public function createMarker( $longitude, $latitude, $title, $content, $enabled = true, $cssclass = "", $decorationMappy = true, Icon $icon = null )
    {
        $marker = new Marker( $longitude, $latitude );
        $marker->setTitle( $title )->setContent( $content );
        if ( !isset( $icon ) )
            $icon = new Icon();
        $icon->setLabel( $this->iconLabel++ );
        $icon->setAnchor( new POI( 0, 0 ) );
        $marker->setIcon( $icon ); /**/
        $marker->setActive( $enabled );
        $modal = new Modal();
        $modal
                ->setDecoration( $decorationMappy )
                ->setAnchor( new POI( 0, 0 ) )
                ->setCssclass( $cssclass )
                ->setAutoLayout( true )
        ;
        $marker->setModal( $modal );

        return $this->addMarker( $marker );
    }

    /**
     * @param Marker $marker
     * @return $this
     */
    public function addMarker( Marker $marker )
    {
        $this->markers[] = $marker;
        $marker->setContainer( $this->container );
        return $this;
    }

    /**
     * @param bool $movable
     * @param bool $zoom
     * @param string $position
     * @param array $anchor
     * @return $this
     */
    public function createTools( $movable = true, $zoom = true, $position = "rb", $anchor = [0, 5] )
    {
        $this->tools = new Tools();
        $this->tools
                ->setZoom( $zoom )
                ->setMovable( $movable )
                ->setAnchor( new POI( $anchor[0], $anchor[1] ) )
                ->setPosition( $position )
        ;
        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getMarkers()
    {
        return $this->markers;
    }

    /**
     * @param $markers
     * @return $this
     */
    public function setMarkers( $markers )
    {
        $this->markers = $markers;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getZoom()
    {
        return $this->zoom;
    }

    /**
     * @param $zoom
     * @return $this
     */
    public function setZoom( $zoom )
    {
        $this->zoom = $zoom;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCenter()
    {
        return $this->center;
    }

    /**
     * @param $longitude
     * @param $latitude
     * @return $this
     */
    public function setSimpleCenter( $longitude, $latitude )
    {
        return $this->setCenter( new POI( $longitude, $latitude ) );
    }

    /**
     * @param POI $centre
     * @return $this
     */
    public function setCenter( POI $centre )
    {
        $this->center = $centre;
        return $this;
    }

    /**
     * @return bool|Tools
     */
    public function getTools()
    {
        return $this->tools;
    }

    /**
     * @param Tools $tools
     * @return $this
     */
    public function setTools( Tools $tools )
    {
        $this->tools = $tools;
        return $this;
    }

    /**
     * @param ContainerInterface $container
     * @return $this
     */
    public function setContainer( ContainerInterface $container = null )
    {
        parent::setContainer( $container );
        return $this;
    }

    /**
     * @return string
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param $width
     * @return $this
     */
    public function setWidth( $width )
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param $height
     * @return $this
     */
    public function setHeight( $height )
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return bool
     */
    public function getStatic()
    {
        return $this->static;
    }

    /**
     * @param $static
     * @return $this
     */
    public function setStatic( $static )
    {
        $this->static = $static;
        return $this;
    }

    /**
     * @return string
     */
    public function getTrigger()
    {
        return $this->trigger;
    }

    /**
     * @param $trigger
     * @return $this
     */
    public function setTrigger( $trigger )
    {
        $this->trigger = $trigger;
        return $this;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param $class
     * @return $this
     */
    public function setClass( $class )
    {
        $this->class = $class;
        return $this;
    }

    /**
     *
     */
    public function __toString()
    {
        $this->render();
    }

}
