<?php

namespace Zephyr\Geo\Data;

/**
 * Description du Trait CreationStatique
 * Délègue la création d'un objet
 * @author Kanea
 */
trait CreateStatic {
	/**
	 * Réalise la création d'un objet
	 * @param mixed les paramètres sont envoyés sans transformation
	 * @return static
	 */
	static public function create(){
		$class = get_called_class();
		$reflection = new \ReflectionClass($class);
		return $reflection->newInstance(func_get_args());
	}
}