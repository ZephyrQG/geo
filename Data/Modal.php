<?php

namespace Zephyr\Geo\Data;

/**
 * Description de modal
 *
 * @author Nicolas de Marqué
 */

/**
 * Class Modal
 * @package Zephyr\Geo\MappyBundle\Data
 */
class Modal
{
    /* @var POI $ancre */

    protected $anchor;

    /**
     * @var bool
     */
    protected $decoration = true; // {boolean}, // Do you want to use mappy style? (default true)
    /**
     * @var
     * {boolean}
     * Do you want to reposition the popup automatically in your map?
     */
    protected $autoLayout;

    /**
     * @var string
     * {number} or "auto", // Distance between the popup anchor and the left of your popUp ("auto" will calculate the middle width of your popup)
     */
    protected $left = "'auto'";

    /**
     * @var string
     */
    protected $top = "'auto'"; //top: {number} or "auto", // Distance between the popup anchor and the top of your popUp ("auto" will calculate the middle height of your popup)
    /**
     * @var string
     */
    protected $right = "'auto'"; //right:
    /**
     * @var string
     */
    protected $bottom = "'auto'";

    /**
     * @var string
     */
    protected $content = "";

    /**
     * @var string
     */
    protected $cssclass = "";

    /**
     * @return POI
     */
    public function getAnchor()
    {
        return $this->anchor;
    }

    /**
     * @param POI $anchor
     * @return $this
     */
    public function setAnchor( POI $anchor )
    {
        $this->anchor = $anchor;
        return $this;
    }

    /**
     * @return bool
     */
    public function getDecoration()
    {
        return $this->decoration;
    }

    /**
     * @param $decoration
     * @return $this
     */
    public function setDecoration( $decoration )
    {
        $this->decoration = $decoration;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAutoLayout()
    {
        return $this->autoLayout;
    }

    /**
     * @param $autoLayout
     * @return $this
     */
    public function setAutoLayout( $autoLayout )
    {
        $this->autoLayout = $autoLayout;
        return $this;
    }

    /**
     * @return string
     */
    public function getLeft()
    {
        return $this->left;
    }

    /**
     * @param $left
     * @return $this
     */
    public function setLeft( $left )
    {
        $this->left = $left;
        return $this;
    }

    /**
     * @return string
     */
    public function getTop()
    {
        return $this->top;
    }

    /**
     * @param $top
     * @return $this
     */
    public function setTop( $top )
    {
        $this->top = $top;
        return $this;
    }

    /**
     * @return string
     */
    public function getRight()
    {
        return $this->right;
    }

    /**
     * @param $right
     * @return $this
     */
    public function setRight( $right )
    {
        $this->right = $right;
        return $this;
    }

    /**
     * @return string
     */
    public function getBottom()
    {
        return $this->bottom;
    }

    /**
     * @param $bottom
     * @return $this
     */
    public function setBottom( $bottom )
    {
        $this->bottom = $bottom;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param $content
     * @return $this
     */
    public function setContent( $content )
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string
     */
    public function getCssclass()
    {
        return $this->cssclass;
    }

    /**
     * @param $classecss
     * @return $this
     */
    public function setCssclass( $classecss )
    {
        $this->cssclass = $classecss;
        return $this;
    }

}
