<?php

namespace Zephyr\Geo\Data;

/**
 * Description de la class icon
 *
 * @author Kanea
 */

/**
 * Class Icon
 * @package Zephyr\Geo\MappyBundle\Data
 */
class Icon
{

    /**
     * @var string
     */
    protected $cssclass = "default-icon";

    /* @var POI $ancre */
    protected $anchor;

    /**
     * @var
     */
    protected $label;

    /**
     * @var int
     */
    protected $width = 21;

    /**
     * @var int
     */
    protected $height = 31;

    /**
     * @var string
     */
    protected $imageUrl = "http://axe.mappy.com/Sources/API/ajax/2.16/img/poi/POI_defaut.png";

    /**
     * @return string
     */
    public function getCssclass()
    {
        return $this->cssclass;
    }

    /**
     * @param $cssclass
     * @return $this
     */
    public function setCssclass( $cssclass )
    {
        $this->cssclass = $cssclass;
        return $this;
    }

    /**
     * @return POI
     */
    public function getAnchor()
    {
        return $this->anchor;
    }

    /**
     * @param $anchor
     * @return $this
     */
    public function setAnchor( $anchor )
    {
        $this->anchor = $anchor;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param $label
     * @return $this
     */
    public function setLabel( $label )
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param $width
     * @return $this
     */
    public function setWidth( $width )
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param $height
     * @return $this
     */
    public function setHeight( $height )
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @param $imageurl
     * @return $this
     */
    public function setImageUrl( $imageurl )
    {
        $this->imageUrl = $imageurl;
        return $this;
    }

}
