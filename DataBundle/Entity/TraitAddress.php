<?php

namespace Zephyr\Geo\DataBundle\Entity;

/**
 * Description of TraitAddress
 *
 * @author Nicolas
 */
Trait TraitAddress
{

    /** @ORM\Column(type="string", length="255" nullable=true) */
    private $address;

    /** @ORM\Column(type="string", length="20", nullable=true) */
    private $zip;

    /** @ORM\Column(type="string", length="150", nullable=true) */
    private $city;

    /** @ORM\Column(type="string", length="10", nullable=true) */
    private $country;
    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    public function getCity()
    {
        return $this->city;
    }

    /**
     * The ISO code
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $address
     * @return $this
     */
    public function setAddress( $address )
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @param string $zip the zip code
     * @return $this
     */
    public function setZip( $zip )
    {
        $this->zip = $zip;
        return $this;
    }

    /**
     * @param string $city the city name
     * @return $this
     */
    public function setCity( $city )
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @param string $country the iso code
     * @return $this
     */
    public function setCountry( $country )
    {
        $this->country = $country;
        return $this;
    }

}