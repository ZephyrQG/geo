<?php

/**
 * Description of Geo
 *
 * @author Nicolas
 */

namespace Zephyr\Geo\DataBundle\Entity;

class Geo
{

    use TraitGeoCoordinates;

    /**
     * @param $longitude
     * @param $latitude
     */
    public function __construct( $longitude, $latitude )
    {
        $this->setLongitude( $longitude );
        $this->setLatitude( $latitude );
    }

}

?>
