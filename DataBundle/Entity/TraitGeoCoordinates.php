<?php

namespace Zephyr\Geo\DataBundle\Entity;

/**
 * Description of TraitGeo
 *
 * @author Nicolas
 */
Trait TraitGeoCoordinates
{

    /** @ORM\Column(type="float", nullable=true) */
    private $longitude;

    /** @ORM\Column(type="float", nullable=true) */
    private $latitude;

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param $longitude
     * @return $this
     */
    public function setLongitude( $longitude )
    {
        $this->longitude = $longitude;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param $latitude
     * @return $this
     */
    public function setLatitude( $latitude )
    {
        $this->latitude = $latitude;
        return $this;
    }

}