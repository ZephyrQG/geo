<?php

namespace Zephyr\Geo\GeolocateBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use GeoIp2\Database\Reader as GeoDatabaseReader;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 * @package Zephyr\Geo\GeolocateBundle\Controller
 * @Route("/geolocate")
 */
class DefaultController extends ContainerAware
{

    /**
     * @Route("/")
     */
    public function anonymousGeolocate()
    {
        $clientIp = $this->container->get('request')->getClientIp();
        $reader = new GeoDatabaseReader(__DIR__."/../Resources/database/GeoLite2-City.mmdb");
        $record = $reader->city("89.2.178.224");
        $cityName = $record->city->name;

        $cookie = new Cookie("location", $cityName, time()+24*60*60);
        $response = new Response();
        $response->headers->setCookie($cookie);
        $response->send();
    }

}
