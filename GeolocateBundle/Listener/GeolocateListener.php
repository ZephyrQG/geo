<?php
/**
 * Created by PhpStorm.
 * User: Arnaud
 * Date: 29/07/13
 * Time: 09:56
 */

namespace Zephyr\Geo\GeolocateBundle\Listener;

use Symfony\Component\DependencyInjection\SimpleXMLElement;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpFoundation\Response;
use GeoIp2\Database\Reader as GeoDatabaseReader;
use GeoIp2\WebService\Client as GeoClient;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Class GeolocateListener
 * @package Zephyr\Geo\GeolocateBundle\Listener
 *
 * 89.2.178.224 home at Caen
 */
class GeolocateListener
{

    private $oneHour;
    private $oneMonth;
    private $apikey;

    /**  */
    public function __construct()
    {
        $this->oneHour = time()+60*60;
        $this->oneMonth = time()+60*60*24*30;
        $this->apikey = "89f369e88d397663";
    }

    /**
     * @param string  $clientIp
     * @param Request $request
     */
    public function locateGeoLite2($clientIp, $request)
    {
        $reader = new GeoDatabaseReader(__DIR__."/../Resources/database/GeoLite2-City.mmdb");
        $record = $reader->city($clientIp);
        $session = $request->getSession();

        $session->set("location", "undefined");
        $session->set("locationLifetime", $this->oneHour);

        $cityName = $record->city->name;

        if (!is_null($cityName)) {
            $session->set("location", $cityName);
            $session->set("locationLifetime", $this->oneMonth);
        }
    }

    /**
     * Locate using geolookup autoip from Wunderground
     * Documentation can be found here : http://www.wunderground.com/weather/api/d/docs?d=data/geolookup
     *
     * @param Request $request
     */
    public function locateWunderground($request)
    {
        $session = $request->getSession();
        $url = "http://api.wunderground.com/api/".$this->apikey."/geolookup/q/autoip.xml";
        $geolocate = file_get_contents($url);

        $session->set("location", "undefined");
        $session->set("locationLifetime", $this->oneHour);

        $response = new SimpleXMLElement($geolocate);

        $cityName = (string) $response->location->city;

        if (!is_null($cityName)) {
            $session->set("location", $cityName);
            $session->set("locationLifetime", $this->oneMonth);
        }
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {

        if (HttpKernelInterface::MASTER_REQUEST === $event->getRequestType()) {
            $request = $event->getRequest();
            $session = $request->getSession();
            $clientIp = $request->getClientIp();
            if ($clientIp == "33.33.33.10" || "127.0.0.1") {
                $clientIp = "109.190.52.207";
            }

            $location = $session->get("location");
            $locationLifetime = $session->get("locationLifetime");

            if ($location == "undefined" && $locationLifetime < time() || $location == null) {
//                $this->locateGeoLite2($clientIp, $request);
                $this->locateWunderground($request);
            }
        }
    }

}