<?php

namespace Zephyr\Geo\MappyBundle\Service;

use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Description of Twig
 *
 * @author Nicolas
 */
class Twig extends \Twig_Extension {
	
	protected $config;
	/* @var ContainerInterface $container */
	protected $container;
	
	public function __construct(ContainerInterface $container, $configuration)
	{
		$this->config = $configuration;
		$this->container = $container;
	}
	
	public function getFilters()
	{
		return array(
			'mappyLib' => new \Twig_Filter_Method($this, 'mappyLib', ['needs_environment'=> false, 'is_safe' => array('html')]),
		);
	}

	public function getFunctions()
	{
		return array(
			'mappyLib' => new \Twig_Function_Method($this, 'mappyLib', ['needs_environment'=> false, 'needs_context'=>true, 'is_safe'=>array('html')]),
		);
	}

	public function mappyLib($tag = true)
	{
		$url = $this->container->get('zephyr.geo.mappy')->getJavascriptLibraryUrl();
		return $tag ? '<script type="text/javascript" src="'.$url.'"></script>' : $url;
	}
	public function getName()
	{
		return "zephyr.geo.mappy.twig.extension";
	}
}

?>
