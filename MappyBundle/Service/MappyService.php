<?php

namespace Zephyr\Geo\MappyBundle\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\Request;
use Zephyr\Geo\DataBundle\Entity\Geo;
use Zephyr\Geo\Data\Map;

/**
 * Description of MappyService
 *
 * @author Nicolas de Marqué
 */
class MappyService
{
	protected $config;
	/* @var ContainerInterface $container */
	protected $container;
	protected $token=false;
	
	public function __construct(ContainerInterface $container, $configuration)
	{
		$this->config = $configuration;
		$this->container = $container;
	}
	/**
	 * @return \Zephyr\Geo\Data\Map
	 */
	public function createMap()
	{
		return Map::create()->setContainer($this->container);
	}
	
	public function getGeolocalisation($address, $all=false)
	{
		$result = json_decode( file_get_contents('http://axe.mappy.com/1v2/'.$this->config['auth']['id'].'/loc/get.aspx?'.
			'opt.format=json&'.
			'opt.namedPlaceSearch=1&'.
			'opt.interactive=1&'.
			'opt.language=FRE&'.
			'opt.xmlOutput=3v0&'.
			'fullAddress='.  urlencode($address).'&'.
			'auth='.$this->getToken()));

		$geos=[];
		if(is_array($result->kml->Document->Placemark)){
			foreach($result->kml->Document->Placemark as $placemark){
				list($longitude, $latitude) = explode (',', $placemark->Point->coordinates);
				$geos[] = new Geo($longitude, $latitude);
			}
		}else{
			list($longitude, $latitude) = explode (',', $result->kml->Document->Placemark->Point->coordinates);
			$geos[] = new Geo($longitude, $latitude);
		}		
		return $all ? $geos : $geos[0];
	}
	
	public function getJavascriptLibraryUrl()
	{
		return 'http://axe.mappy.com/1v1/init/get.aspx?auth='.$this->getToken().'&version='.$this->config['version'].'&solution=ajax';
	}
	
	public function getToken()
	{
		if( ! function_exists( __NAMESPACE__ . "\getToken" )){
			function getToken($login, $pwd, $ip) { 
				$ACCESSOR_URL = 'http://axe.mappy.com/1v1/'; 
				$timestamp = time();
				$hash = md5($login."@".$pwd."@".$timestamp);
				$preToken =  $login."@".$timestamp."@".$hash;					
				$urlGetToken = $ACCESSOR_URL . 'token/generate.aspx?auth=' . urlencode($preToken) . '&ip=' . $ip;
				return file_get_contents($urlGetToken);
			}
		}

		if(isset($_SESSION['mappy_token']))
			$this->token = $_SESSION['mappy_token'];
		
		$token = $this->token
		  ? $this->token 
		  : getToken(
			$this->config['auth']['id'],
			$this->config['auth']['code'],
			$this->getExternalIP());

		$_SESSION['mappy_token'] = $token;
		
		if (!$token) {
			throw new \Exception("Token don't work");
		}
		return $token;
	}

	public function getExternalIP()
	{
		return file_get_contents('http://api.exip.org/?call=ip');
	}
	
}