var search = function (b8,ce,ci,cf){
	 var cg,j;var ch;var cc;var ca;var b9;var cb;
	 var cd=function(cj){
		 var i;try{
			 if(cj.photo.photoSearch.answer.nav.type==="frontage"){
				 i=new ae(cj.photo.photoSearch.answer)
			 }else{
				 if(cj.photo.photoSearch.answer.nav.type==="sequence"){
					 i=new aR(cj.photo.photoSearch.answer)
				 }
			 }
		 }catch(ck){
			 if(R(cf)){
				 cf(ck)
			 }else{
				 throw ck
			 }
			 return
		 }
		 ci(i)
	 };
	 if(b8&&b8.officialTownCode&&b8.countryNameCode&&b8.wayName&&b8.number){
		 for(cb=0;cb<s.length;cb+=1){
			 if(s[cb].CountryCode===b8.countryNameCode&&s[cb].Order08===b8.officialTownCode){
				 ch=s[cb].Code;break
			 }
		 }
		 if(ch){
			 cg=new bs();
			 cg.configure({townCode:ch,countryCode:b8.countryNameCode,number:b8.number,wayName:b8.wayName,objectType:ce});
			 an(cg,cd)
		 }else{
			 cf(new Error("Town not found."))
		 }
	 }else{
		 if(b8&&b8.Placemark&&b8.Placemark.AddressDetails&&b8.Placemark.AddressDetails.Country&&b8.Placemark.AddressDetails.Country.CountryNameCode&&b8.Placemark.AddressDetails.Country.CountryNameCode.value&&b8.Placemark.AddressDetails.Country.AdministrativeArea&&b8.Placemark.AddressDetails.Country.AdministrativeArea.Locality&&b8.Placemark.AddressDetails.Country.AdministrativeArea.Locality.Thoroughfare&&b8.Placemark.AddressDetails.Country.AdministrativeArea.Locality.Thoroughfare.ThoroughfareNumber&&b8.Placemark.ExtendedData["mappy:OfficialTownCode"]){
			 for(cb=0;cb<s.length;cb+=1){
				 if(s[cb].CountryCode===b8.Placemark.AddressDetails.Country.CountryNameCode.value&&s[cb].Order08===b8.Placemark.ExtendedData["mappy:OfficialTownCode"]&&b8.Placemark.AddressDetails.Country.AdministrativeArea.Locality.Thoroughfare){
					 ch=s[cb].Code;
					 cc=s[cb].CountryCode;
					 ca=b8.Placemark.AddressDetails.Country.AdministrativeArea.Locality.Thoroughfare.ThoroughfareNumber.Requested||b8.Placemark.AddressDetails.Country.AdministrativeArea.Locality.Thoroughfare.ThoroughfareNumber.Interpolated;
					 b9=b8.Placemark.AddressDetails.Country.AdministrativeArea.Locality.Thoroughfare.ThoroughfareName
				 }
			 }
			 if(ch&&cc&&b9&&ca){
				 cg=new bs();
				 cg.configure({townCode:ch,countryCode:cc,number:ca,wayName:b9,objectType:ce});
				 an(cg,cd)
			 }else{
				 if(typeof cf==="function"){
					 cf(new Error("Not enough information."))
				 }
			 }
		 }else{
			 if(b8 instanceof b5){
				 cg=new bs();
				 ce=b8.getType();
				 if(ce==="Facades"){
					 j="f"
				 }else{
					 if(ce==="Sequences"){
						 j="s"
					 }
				 }
				 cg.configure({id:b8.getId(),objectType:j,view:b8.getView()});
				 an(cg,cd)
			 }else{
				 if(b8 instanceof af){
					 cg=new bs();
					 j="s";
					 cg.configure({id:b8.getId(),objectType:j,view:b8.getView()});
					 an(cg,cd)
				 }else{
					 if(b8 instanceof b3){
						 cg=new bs();
						 cg.configure({coordinates:b8,objectType:ce});
						 an(cg,cd)
					 }else{
						 if(typeof cf==="function"){
							 cf(new Error("Not enough information."))
						 }
					 }
				 }
			 }
		 }
	 }
}
var townHasPhotos = function (b9,j){for(var b8 in s){if(s.hasOwnProperty(b8)){if(s[b8].CountryCode===b9&&s[b8].Order08===j){return true}}}return false}
