<?php

namespace Zephyr\Geo\MappyBundle\Controller;

use Zephyr\Geo\MappyBundle\Service\MappyService;
use Zephyr\Geo\Data\POI;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/mappy")
 */
class DefaultController extends Controller
{

    /**
     * @Route("/test")
     * @Template()
     */
    function testAction( $test = null )
    {
        /* @var $mappyService MappyService */
        $mappyService = $this->get( 'zephyr.geo.mappy' );
        return [
            'requete' => $req = "Longue Vue des Photographes 14111 Louvigny",
            'resultat' => $mappyService->getGeolocalisation( $req )
        ];
    }

    /**
     * @Route("/test/PhotoService")
     * @Template()
     */
    function testPhotoServiceAction( $test = null )
    {
        /* var $mappyService MappyService */
        $mappyService = $this->get( 'zephyr.geo.mappy' );
        return [];
    }

    /**
     * @Route("/test/CarteAuto")
     * @Template()
     */
    function testCarteAutoAction( $test = null )
    {
        /* @var $carte \Zephyr\Geo\Data\Map */
        $carte = $this->get( 'zephyr.geo.mappy' )->createMap();
        $carte
            ->createTools()
            ->setCenter( $p = new POI( 0, 0 ) )
            ->setZoom( 7 )
            ->createMarker( $p->getX(), $p->getY(), "title", "content" );

        return ['carte' => $carte];
    }

}
